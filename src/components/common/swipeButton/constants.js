import { Dimensions } from 'react-native';

export const DEFAULT_HEIGHT = 70;
export const DEFAULT_WIDTH = Dimensions.get('window').width * 0.9;
export const DEFAULT_BORDER_RADIUS = DEFAULT_HEIGHT / 2;
export const DEFAULT_COMPLETE_THRESHOLD_PERCENTAGE = 90;
