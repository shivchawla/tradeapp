export const AccountStatus = {
	SUBMITTED: 0,
	ACTION_REQUIRED: 1,
	APPROVAL_PENDING: 2,
	APPROVED: 3,
	REJECTED: 4,
	ACTIVE: 5,
	DISABLED: 6,
	ACCOUNT_CLOSE: 7
};

export const EmploymentStatus = {
	unemployed:	0,
	employed: 1,
	student: 2,
	retired: 3
}

export const Document = {
	unemployed:	0,
	employed: 1,
	student: 2,
	retired: 3
}

